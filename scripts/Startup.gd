extends Control


var xdirection = 1
var ydirection = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	$Knight.paralyzed = true
	#$Knight/Sprite.play("run")


# Called every frame. 'delta' is the elapsed time since the previous frame.

func _process(_delta):
	if $Knight.position.x > (320 + $Knight/CollisionShape2D.shape.radius * 2):
		xdirection = -1
		ydirection = rand_range(-1, 1)
	elif $Knight.position.x < (0 - $Knight/CollisionShape2D.shape.radius * 2):
		xdirection = 1
		ydirection = rand_range(-1, 1)
	
	if $Knight.position.y > (180 + $Knight/CollisionShape2D.shape.radius * 2):
		ydirection = -ydirection
	elif $Knight.position.y < (0 - $Knight/CollisionShape2D.shape.radius * 2):
		ydirection = -ydirection
		
	$Knight.move(Vector2(xdirection * $Knight.MAX_SPEED, ydirection * $Knight.MAX_SPEED))

