extends KinematicBody2D

export (bool) var paralyzed = false

export var ACCELERATION = 400
export var FRICTION = 300
export var MAX_SPEED = 64
var velocity = Vector2.ZERO

enum {
	MOVE,
	ATTACK,
	HIT
}

var state = MOVE

func _physics_process(delta):
	match state:
		MOVE:
			state_move(delta)
		ATTACK:
			pass
		HIT:
			pass
		
func state_move(delta):
	if !paralyzed:
		var movement = Vector2.ZERO
		
		movement.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		movement.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
		movement = movement.normalized()
		
		if movement != Vector2.ZERO:
			velocity = velocity.move_toward(movement * MAX_SPEED, ACCELERATION * delta)
		else:
			velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	
	velocity = move_and_slide(velocity)
	#if collision:
	#	print("Collision:" + collision)
	#	velocity = Vector2.ZERO
	
	if velocity.x < 0:
		$Sprite.flip_h = true
	elif velocity.x > 0:
		$Sprite.flip_h = false
		
	
	if velocity.x == 0 and velocity.y == 0:
		$Sprite.play("idle")
	else:
		$Sprite.play("run")

func move(offset):
	velocity = offset


func _on_Sprite_animation_finished():
	match $Sprite.animation:
		"attack":
			state = MOVE
